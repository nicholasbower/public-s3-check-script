#! /usr/bin/env python3
import boto3


# boto resource is needed to list all buckets in the aggregate_buckets function
s3 = boto3.resource('s3')

#boto client is used to interact with each bucket and is used in the bucket_policy_lookup function
client = boto3.client('s3')


#Function used to lookup bucket policy by name - returns True if bucket is public, False, if not Public or if it errors out
def bucket_policy_lookup(bucket_name):
    try:
        res = client.get_bucket_policy_status(Bucket=bucket_name)
        isPublic = res['PolicyStatus']['IsPublic']
        return isPublic
    except Exception as e:
        #Defaults to false because an error means we don't have permissions to access
        return False


# Initial function call - reads each bucket from AWS - passes the name to the bucket_policy_lookup function - and appends it to a new list
# The all_buckets list is used to store the bucket name and private/public status
# This is done so that we can associate the bucket name with its status, this info does not come from AWS so we have to make it
def aggregate_buckets():
    all_buckets = []
    for bucket in s3.buckets.all():
        #If the policy check errors out - default is to False
        policy_status = bucket_policy_lookup(bucket.name) 

        #Append each bucket name and status to a list 
        all_buckets.append({"name": bucket.name, "is_public": policy_status})
    
    return all_buckets


# Utility function that takes all buckets returned in the aggregate buckets function above, and returns only items where the is_public key is True
# Please note - the lambda used on line 40 is a Python lambda (anonmyous function) and has NOTHING to do with AWS lambdas
def filter_public(buckets):
    return filter(lambda is_public: is_public['is_public'], buckets)


# main check -- if True, the conditional body will run
if __name__ == "__main__":
    all_buckets = aggregate_buckets()
    print(list(filter_public(all_buckets)))